package com.uppalapati.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.uppalapati.ui.drivers.model.Driver
import com.uppalapati.ui.route.model.Route

@Dao
interface DriverDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDriver(driver: Driver)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRoute(route: Route)

    @Query("select * from driver_table ORDER BY name ASC")
    fun getAllDrivers(): LiveData<List<Driver>>

    @Query("select * from route_table WHERE id = :driverId")
    fun getAllRoutes(driverId:String): LiveData<List<Route>>

    @Query("select * from route_table WHERE type = :type")
    fun getSelectedRoutes(type:String): LiveData<List<Route>>
}