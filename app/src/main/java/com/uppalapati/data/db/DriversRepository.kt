package com.uppalapati.data.db

import android.app.Application
import androidx.lifecycle.LiveData
import com.uppalapati.ui.drivers.model.Driver
import com.uppalapati.ui.route.model.Route

class DriversRepository (application: Application) {

    private var driverDao: DriverDao
    private var allDrivers: LiveData<List<Driver>>

    private val database = DriversDatabase.getInstance(application)

    init {
        driverDao = database.driversDao()
        allDrivers = driverDao.getAllDrivers()
    }

    fun insertDriver(driver: Driver) {
        driverDao.insertDriver(driver)
    }

    fun insertRoute(route: Route) {
        driverDao.insertRoute(route)
    }

    fun getAllDrivers(): LiveData<List<Driver>> {
        return driverDao.getAllDrivers()
    }

    fun getAllRoutes(driverId:String): LiveData<List<Route>> {
        return driverDao.getAllRoutes(driverId)
    }

    fun getSelectedRouteTypes(type:String): LiveData<List<Route>> {
        return driverDao.getSelectedRoutes(type)
    }

}