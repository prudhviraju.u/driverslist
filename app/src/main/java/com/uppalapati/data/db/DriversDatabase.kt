package com.uppalapati.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.uppalapati.ui.drivers.model.Driver
import com.uppalapati.ui.route.model.Route

@Database(entities = [Driver::class, Route::class], version = 1)
abstract class DriversDatabase : RoomDatabase() {
    abstract fun driversDao(): DriverDao

    companion object {
        private var instance: DriversDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): DriversDatabase {
            if(instance == null) {
                instance = Room.databaseBuilder(
                    ctx.applicationContext, DriversDatabase::class.java,
                    "drivers_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance!!

        }
    }
}