package com.uppalapati.data.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitBuilder {
    companion object {
        private var instance : ApiService? = null

        @Synchronized
        fun getInstance(): ApiService {
            if (instance == null)
                instance = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://d49c3a78-a4f2-437d-bf72-569334dea17c.mock.pstmn.io/")
                    .build()
                    .create(ApiService::class.java)
            return instance as ApiService
        }
    }

}