package com.uppalapati.data.api

import com.uppalapati.ui.drivers.model.Drivers
import retrofit2.http.GET

interface ApiService {

    @GET("data")
    suspend fun getDriverList(): Drivers
}