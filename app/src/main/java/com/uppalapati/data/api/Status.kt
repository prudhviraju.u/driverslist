package com.uppalapati.data.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}