package com.uppalapati.ui.drivers.view

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.uppalapati.data.api.Status
import com.uppalapati.data.db.DriversRepository
import com.uppalapati.databinding.ActivityDriverBinding
import com.uppalapati.shared.base.BaseActivity
import com.uppalapati.ui.drivers.model.Driver
import com.uppalapati.ui.drivers.viewmodel.DriversViewModel

class DriversActivity : BaseActivity() {

    private lateinit var mBinding: ActivityDriverBinding
    private lateinit var driversViewModel: DriversViewModel
    private lateinit var adapter : DriversListAdapter
    private lateinit var driversRepository: DriversRepository
    private var driversList = ArrayList<Driver>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityDriverBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        driversRepository = DriversRepository(application)
        setupViewModel()
        setupRecyclerView()
        setupObservers()
    }

    private fun setupViewModel() {
        driversViewModel = DriversViewModel(driversRepository, this)
    }

    private fun setupRecyclerView() {
        adapter =  DriversListAdapter(driversList,this)
        mBinding.driversList.layoutManager = LinearLayoutManager(applicationContext)
        mBinding.driversList.adapter = adapter
    }

    private fun setupObservers() {
        driversViewModel.getDriversData().observe(this) {
            when (it.status) {
                Status.SUCCESS -> {
                    hideProgressBar()
                    it.data?.let { drivers ->
                        driversRepository.getAllDrivers().observe(this){
                            driversList.addAll(it)
                            adapter.notifyDataSetChanged()
                        }
                    }
                }
                Status.LOADING -> {
                    showProgressBar()
                }
                else -> {
                    hideProgressBar()
                }
            }
        }
    }
}