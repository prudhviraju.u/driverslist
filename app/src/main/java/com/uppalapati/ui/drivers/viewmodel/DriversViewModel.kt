package com.uppalapati.ui.drivers.viewmodel

import androidx.lifecycle.*
import com.uppalapati.data.api.Resource
import com.uppalapati.data.api.RetrofitBuilder
import com.uppalapati.data.db.DriversRepository
import com.uppalapati.ui.drivers.model.Driver
import com.uppalapati.ui.drivers.model.Drivers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class DriversViewModel(private val repository: DriversRepository,
                       private val lifecycleOwner: LifecycleOwner) : ViewModel() {

    private val driversData = MutableLiveData<Resource<List<Driver>>>()


    init {
        setDriversList()
    }

    private fun setDriversList(){
        viewModelScope.launch(Dispatchers.IO) {
            val retrofit = RetrofitBuilder.getInstance()
            driversData.postValue(Resource.loading(null))
            try {
                val driverList = retrofit.getDriverList()
                insert(driverList)
                driversData.postValue(Resource.success(driverList.drivers))
            }catch (e:Exception){
                driversData.postValue(Resource.error(e.toString(), null))
            }
        }
    }

    private fun insert(drivers: Drivers) {
        drivers.drivers.forEach {
            repository.insertDriver(it)
        }
        drivers.routes.forEach {
            repository.insertRoute(it)
        }
    }

    fun getDriversData(): LiveData<Resource<List<Driver>>> {
        return driversData
    }
}