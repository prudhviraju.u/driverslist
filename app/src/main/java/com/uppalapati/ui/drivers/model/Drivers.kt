package com.uppalapati.ui.drivers.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.uppalapati.ui.route.model.Route

data class Drivers(
    val drivers: List<Driver>,
    val routes: List<Route>
)

@Entity(tableName = "driver_table")
data class Driver(
    @PrimaryKey  @NonNull val id: String,
    @ColumnInfo val name: String
)