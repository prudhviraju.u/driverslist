package com.uppalapati.ui.drivers.view

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.uppalapati.R
import com.uppalapati.ui.drivers.model.Driver
import com.uppalapati.ui.route.view.RouteActivity
import com.uppalapati.utils.ID

class DriversListAdapter(private val dataSet: List<Driver>,
                         private val context:Activity) :
    RecyclerView.Adapter<DriversListAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val driverName: TextView

        init {
            // Define  listener for the ViewHolder's View.
            driverName = view.findViewById(R.id.driver_name)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_driver_list, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.driverName.text = dataSet[position].name
        viewHolder.itemView.setOnClickListener {
            val intent = Intent(context,RouteActivity::class.java)
            intent.putExtra(ID,dataSet[position].id)
            context.startActivity(intent)
        }
    }

    override fun getItemCount() = dataSet.size

}