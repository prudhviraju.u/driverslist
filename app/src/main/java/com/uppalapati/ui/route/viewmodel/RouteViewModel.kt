package com.uppalapati.ui.route.viewmodel

import androidx.lifecycle.*
import com.uppalapati.data.db.DriversRepository
import com.uppalapati.ui.route.model.Route
import com.uppalapati.ui.route.model.RouteTypes
import kotlinx.coroutines.async

class RouteViewModel(private val repository: DriversRepository,
                     driverId:String, private val lifecycleOwner: LifecycleOwner) : ViewModel() {
    private val routeListData = MutableLiveData<List<Route>>()

    init {
        getRouteList(driverId)
    }

    private fun getRouteList(driverId: String){
        viewModelScope.async {
            repository.getAllRoutes(driverId = driverId)
                .observe(lifecycleOwner,
                    onChanged = { routeList ->
                        if(routeList.isNotEmpty()) {
                            routeListData.postValue(routeList)
                        }else if(driverId.toInt() % 2 == 0){
                            getSelectedList(type = RouteTypes.R.name)
                        }else if(driverId.toInt() % 5 == 0){
                            getSelectedList(type = RouteTypes.C.name)
                        }else{
                            getSelectedList(type = RouteTypes.C.name)
                        }
                    })
        }
    }

    private fun getSelectedList(type:String){
        repository.getSelectedRouteTypes(type = type)
            .observe(lifecycleOwner,
                onChanged = { routeList ->
                    routeListData.postValue(routeList)
                })
    }

    fun getRouteList():LiveData<List<Route>>{
        return routeListData
    }
}