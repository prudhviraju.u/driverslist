package com.uppalapati.ui.route.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "route_table")
data class Route(
    @PrimaryKey  @NonNull val id: Int,
    @ColumnInfo val name: String,
    @ColumnInfo val type: String
)
