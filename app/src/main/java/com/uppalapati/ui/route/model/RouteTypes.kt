package com.uppalapati.ui.route.model

enum class RouteTypes {
    R,
    C,
    I
}