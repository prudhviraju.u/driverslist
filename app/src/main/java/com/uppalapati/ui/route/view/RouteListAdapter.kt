package com.uppalapati.ui.route.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.uppalapati.R
import com.uppalapati.ui.route.model.Route

class RouteListAdapter(private val dataSet: List<Route>) :
    RecyclerView.Adapter<RouteListAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val routeName: TextView
        val routeType: TextView
        init {
            routeName = view.findViewById(R.id.route_name)
            routeType = view.findViewById(R.id.route_type)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_route_list, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val context = viewHolder.itemView.context
        viewHolder.routeName.text = context.getString(R.string.route_name,dataSet[position].name)
        viewHolder.routeType.text = context.getString(R.string.route_type, dataSet[position].type)
    }

    override fun getItemCount() = dataSet.size

}