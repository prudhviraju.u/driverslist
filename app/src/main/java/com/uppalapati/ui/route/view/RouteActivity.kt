package com.uppalapati.ui.route.view

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.uppalapati.data.db.DriversRepository
import com.uppalapati.databinding.ActivityRouteBinding
import com.uppalapati.shared.base.BaseActivity
import com.uppalapati.ui.route.model.Route
import com.uppalapati.ui.route.viewmodel.RouteViewModel
import com.uppalapati.utils.ID

class RouteActivity : BaseActivity()  {

    private lateinit var mBinding: ActivityRouteBinding
    private lateinit var routeViewModel: RouteViewModel
    private lateinit var adapter : RouteListAdapter
    private lateinit var driversRepository: DriversRepository
    private var routeList = ArrayList<Route>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = ActivityRouteBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        driversRepository = DriversRepository(application)
        setupViewModel()
        setupRecyclerView()
        setupObservers()
    }

    private fun setupViewModel() {
        routeViewModel = RouteViewModel(driversRepository,
            intent.getStringExtra(ID)?:"",
            this
        )
    }

    private fun setupRecyclerView() {
        adapter =  RouteListAdapter(routeList)
        mBinding.routeList.layoutManager = LinearLayoutManager(applicationContext)
        mBinding.routeList.adapter = adapter
    }

    private fun setupObservers() {
        routeViewModel.getRouteList().observe(this) {
            it?.let { routes->
                routeList.addAll(routes)
                adapter.notifyDataSetChanged()
            }
        }
    }
}